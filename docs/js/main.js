
// 1° List, 1° E to show.


var li_elements = document.querySelectorAll(".contcategorias ul li");
var itemc_elements = document.querySelectorAll(".itemc");

for ( var i = 0; i < li_elements.length; i++) {

  li_elements[i].addEventListener("click", function(){
        li_elements.forEach(function(li) {
              li.classList.remove("active");
        })

        this.classList.add("active");
        var li_value = this.getAttribute("data-li");

        itemc_elements.forEach(function(itemc) {
            itemc.style.display = "none";
        })

        if(li_value == "en") {
            document.querySelector("." + li_value).style.display = "block";
        }
        else if(li_value == "se"){
            document.querySelector("." + li_value).style.display = "block";
        }

        else if(li_value == "po"){
            document.querySelector("." + li_value).style.display = "block";          
        }         


  })
}


// 2° List, 2° E to show ( Ensaladas )

var li_elements1 = document.querySelectorAll(".contrecetas ul li");
var item_elements1 = document.querySelectorAll(".item1");


for ( var i = 0; i < li_elements1.length; i++) {

  li_elements1[i].addEventListener("click", function(){
        li_elements1.forEach(function(li) {
              li.classList.remove("active");
        })

        this.classList.add("active");
        var li_value1 = this.getAttribute("data-li");

        item_elements1.forEach(function(item1) {
            item1.style.display = "none";
        })

        if(li_value1 == "en1") {
            document.querySelector("." + li_value1).style.display = "block";
        }
        else if(li_value1 == "en2"){
            document.querySelector("." + li_value1).style.display = "block";
        }

        else if(li_value1 == "en3"){
            document.querySelector("." + li_value1).style.display = "block";          
        }
                
  })
}

// 2° List, 2° E to show ( Segundos )

var li_elements2 = document.querySelectorAll(".contrecetas2 ul li");
var item_elements2 = document.querySelectorAll(".item2");


for ( var i = 0; i < li_elements2.length; i++) {

  li_elements2[i].addEventListener("click", function(){
        li_elements2.forEach(function(li) {
              li.classList.remove("active");
        })

        this.classList.add("active");
        var li_value2 = this.getAttribute("data-li");

        item_elements2.forEach(function(item2) {
            item2.style.display = "none";
        })

        if(li_value2 == "se1") {
            document.querySelector("." + li_value2).style.display = "block";
        }
        else if(li_value2 == "se2"){
            document.querySelector("." + li_value2).style.display = "block";
        }

        else if(li_value2 == "se3"){
            document.querySelector("." + li_value2).style.display = "block";          
        }
                
  })
}


// 2° List, 2° E to show ( Postres )

var li_elements3 = document.querySelectorAll(".contrecetas3 ul li");
var item_elements3 = document.querySelectorAll(".item3");


for ( var i = 0; i < li_elements3.length; i++) {

  li_elements3[i].addEventListener("click", function(){
        li_elements3.forEach(function(li) {
              li.classList.remove("active");
        })

        this.classList.add("active");
        var li_value3 = this.getAttribute("data-li");

        item_elements3.forEach(function(item3) {
            item3.style.display = "none";
        })

        if(li_value3 == "po1") {
            document.querySelector("." + li_value3).style.display = "block";
        }
        else if(li_value3 == "po2"){
            document.querySelector("." + li_value3).style.display = "block";
        }

        else if(li_value3 == "po3"){
            document.querySelector("." + li_value3).style.display = "block";          
        }
                
  })
}


